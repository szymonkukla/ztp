﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduler.Models
{
    public class EmailModel
    {
        public string RecipientEmail { get; set; }
        public string RecipientName { get; set; }
        public string Subject { get; set; }
        public string TemplatePath { get; set; }
        public object TemplateModel { get; set; }
    }
}
