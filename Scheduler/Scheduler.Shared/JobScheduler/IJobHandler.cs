﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JobScheduler
{
    public interface IJobHandler
    {
        void ActivateJob(int index);
    }
}
