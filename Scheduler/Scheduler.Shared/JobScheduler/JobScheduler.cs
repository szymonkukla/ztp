﻿using Hangfire;
using Hangfire.MemoryStorage;
using JobScheduler.HangFireProvider;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace JobScheduler
{
    public class JobScheduler<T> where T : IJobHandler
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly JobsProvider<T> _jobsProvider;
        public JobScheduler()
        {
            _serviceProvider = new ServiceCollection()
            .AddLogging()
            .AddHangfire(config => config.UseMemoryStorage())
            .AddScoped<JobsProvider<T>>()
            .BuildServiceProvider();

            GlobalConfiguration.Configuration.UseActivator(new HangFireActivator(_serviceProvider));
            GlobalJobFilters.Filters.Add(new AutomaticRetryAttribute { Attempts = 0 });

            _jobsProvider = _serviceProvider.GetService<JobsProvider<T>>();
        }

        public void RunRecuringJob(T job, int index, string cron = "* * * * *")
        {
            _jobsProvider.RunSyncJob(job, index, cron);
        }
    }
}
