﻿using Hangfire;
using System;
using System.Collections.Generic;
using System.Text;

namespace JobScheduler
{
    public interface IJobScheduler<T> where T : IJobHandler
    {
        BackgroundJobServer RunJobServer();
        void RunRecuringJob(T job, int index, string jobId, string cron = "* * * * *");
    }
}
