﻿using Scheduler.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Scheduler.Interfaces
{
    public interface IClientsDataAccess
    {
        IEnumerable<DiscountMessage> GetAllMessages();
        IEnumerable<DiscountMessage> GetMessagesFromIndex(int index = 0, int maxCount = 0);
    }
}
