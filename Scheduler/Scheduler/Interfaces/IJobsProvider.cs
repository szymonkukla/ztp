﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Scheduler.Interfaces
{
    public interface IJobsProvider
    {
        void UpdateSyncJob(int nextIndex = 0);
        void SyncDiscountsData(int index);
    }
}
