﻿using Scheduler.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Scheduler.Interfaces
{
    public interface IDiscountInfoManager
    {
        Task SendNewDiscounts(IEnumerable<DiscountMessage> messages);
    }
}
