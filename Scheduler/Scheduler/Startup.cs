﻿using CsvRepository;
using EmailSender;
using Hangfire;
using Hangfire.MemoryStorage;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Scheduler.Interfaces;
using Scheduler.Models;
using Scheduler.Models.Config;
using Scheduler.Providers.DoscountInfoManager;
using Scheduler.Providers.HangFireActivator;
using Scheduler.Providers.JobsProvider;
using Serilog;
using System;
using System.IO;
using System.Net;
using System.Net.Mail;

namespace Scheduler
{
    public class Startup
    {
        private CsvSettingsModel _csvSettings = new CsvSettingsModel();
        private SmtpSettingsModel _smtpSettings = new SmtpSettingsModel();

        public Startup(IHostingEnvironment env)
        {
            var Builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);

            Log.Logger = new LoggerConfiguration()
                   .MinimumLevel.Debug()
                   .WriteTo.RollingFile(Path.Combine(env.ContentRootPath, "log-{Date}.txt"))
                   .CreateLogger();

            Configuration = Builder.Build();
        }

        public IConfiguration Configuration { get; }
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddHangfire(config => config.UseMemoryStorage());

            Configuration.GetSection("CsvSettings").Bind(_csvSettings);
            services.AddSingleton(_csvSettings);

            Configuration.GetSection("SmtpSettings").Bind(_smtpSettings);
            services.AddSingleton(_smtpSettings);

            services.AddTransient<IJobsProvider, JobsProvider>();
            services.AddScoped<ICsvRepository<DiscountMessage>, CsvRepository<DiscountMessage>>();
            services.AddScoped<IClientsDataAccess, CsvManager.ClientsDataAccess>();
            services.AddScoped<IDiscountInfoManager, DiscountInfoManager>();
            services.AddScoped<IEmailSender, EmailSender.EmailSender>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IServiceProvider serviceProvider, IJobsProvider jobsProvider, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            loggerFactory.AddSerilog();

            GlobalConfiguration.Configuration.UseActivator(new HangFireActivator(serviceProvider));
            GlobalJobFilters.Filters.Add(new AutomaticRetryAttribute { Attempts = 0 });
            app.UseHangfireServer();

            jobsProvider.UpdateSyncJob();
        }


    }
}
