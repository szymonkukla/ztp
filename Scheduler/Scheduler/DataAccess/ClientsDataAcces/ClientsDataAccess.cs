﻿using CsvHelper;
using CsvRepository;
using Microsoft.Extensions.Configuration;
using Scheduler.Interfaces;
using Scheduler.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Scheduler.CsvManager
{
    public class ClientsDataAccess : IClientsDataAccess
    {
        private ICsvRepository<DiscountMessage> _discRepo;

        public ClientsDataAccess(ICsvRepository<DiscountMessage> discRepo)
        {
            _discRepo = discRepo;
        }

        public IEnumerable<DiscountMessage>GetAllMessages()
        {
            return _discRepo.GetAllRecords();
        }
        public IEnumerable<DiscountMessage> GetMessagesFromIndex(int index = 0, int maxCount = 0)
        {
            return _discRepo.GetRecordsFromIndex(index, maxCount);
        }
    }
}
