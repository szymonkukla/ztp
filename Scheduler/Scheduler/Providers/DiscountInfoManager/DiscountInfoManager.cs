﻿using EmailSender;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Scheduler.Interfaces;
using Scheduler.Models;
using Scheduler.Models.Config;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Scheduler.Providers.DoscountInfoManager
{
    public class DiscountInfoManager : IDiscountInfoManager
    {
        private IClientsDataAccess _clientsDA;
        private IEmailSender _emailProvider;
        private ILogger<IDiscountInfoManager> _logger;
        private SchedulerSettingsModel _settings = new SchedulerSettingsModel();

        public DiscountInfoManager(IClientsDataAccess clientsDA, IEmailSender emailProvider, IConfiguration config, ILogger<IDiscountInfoManager> logger)
        {
            _clientsDA = clientsDA;
            _emailProvider = emailProvider;
            _logger = logger;
            config.GetSection("SchedulerSettings").Bind(_settings);
        }

        public async Task SendNewDiscounts(IEnumerable<DiscountMessage> messages)
        {
            var emails = PrepareMessages(messages);
            await Task.WhenAll(emails.Select(em => _emailProvider.SendEmailAsync(em)));
        }

        private IEnumerable<EmailModel> PrepareMessages(IEnumerable<DiscountMessage> discounts)
        {
            return discounts.Select(d =>
                new EmailModel
                {
                    RecipientEmail = d.Email,
                    RecipientName = $"{d.FirstName} {d.LastName}",
                    Subject = "Mega discount, specially for You!",
                    TemplatePath = "DiscountMailTemplate.cshtml",
                    TemplateModel = d
                }
            );
        }
    }
}
