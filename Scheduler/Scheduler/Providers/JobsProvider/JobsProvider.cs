﻿using Hangfire;
using Hangfire.Storage;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Scheduler.Interfaces;
using Scheduler.Models.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Scheduler.Providers.JobsProvider
{
    public class JobsProvider : IJobsProvider
    {
        private SchedulerSettingsModel _settings = new SchedulerSettingsModel();
        private IClientsDataAccess _clientsDataAccess;
        private IDiscountInfoManager _discountInfoManager;
        private ILogger _logger;
        private readonly string _jobId = "DiscountCheckJob";
        public JobsProvider(IConfiguration config, IClientsDataAccess clientsDataAccess, IDiscountInfoManager discountInfoManager, ILogger<IJobsProvider> logger)
        {
            _clientsDataAccess = clientsDataAccess;
            _discountInfoManager = discountInfoManager;
            _logger = logger;
            config.GetSection("SchedulerSettings").Bind(_settings);
        }

        public void UpdateSyncJob(int nextIndex = 0)
        {
            var isJob = IsJobExist(_jobId);

            if (nextIndex == 0 && isJob)
            {
                _logger.LogInformation($"Job exist: {_jobId}");
                return;
            }
            _logger.LogInformation($"Updating job: {_jobId} with index: {nextIndex}");
            RecurringJob.AddOrUpdate(_jobId, () => SyncDiscountsData(nextIndex), _settings.SendInterval, TimeZoneInfo.Utc);
        }

        public void SyncDiscountsData(int index)
        {
            var clients = _clientsDataAccess.GetMessagesFromIndex(index, _settings.EmailsPerInterval);

            _logger.LogInformation($"Clients count readed: {clients.Count()}");
            if (clients.Any())
            {
                _discountInfoManager.SendNewDiscounts(clients);
                UpdateSyncJob(index + clients.Count());
            }
        }

        private bool IsJobExist(string jobId)
        {
            var isExists = false;
            using (var connection = JobStorage.Current.GetConnection())
            {
                var recurringJobs = connection.GetRecurringJobs();
                isExists = recurringJobs.Any(j => j.Id == jobId && !j.Removed);
            }
            return isExists;
        }
    }
}
