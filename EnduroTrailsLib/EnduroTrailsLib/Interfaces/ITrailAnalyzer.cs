﻿using EnduroTrails.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace EnduroTrailsLib.Interfaces
{
    public interface ITrailAnalyzer
    {
        ICollection<GpxPoint> GetGpxData();
        double GetDistance();
        double GetClimbingDistance();
        double GetDescentDistance();
        double GetFlatDistance();
        double GetMinSpeed();
        double GetMaxSpeed();
        double GetAvgSpeed();
        double GetMinElev();
        double GetAvgElev();
        double GetTotalClimbing();
        double GetTotalDescent();
        double GetFinalBalance();
        TimeSpan GetTotalTrackTime();
        TimeSpan GetClimbingTime();
        TimeSpan GetDescTime();
        TimeSpan GetFlatTime();
    }
}
