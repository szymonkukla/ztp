﻿using EnduroTrails.DataReader;
using EnduroTrails.Models;
using EnduroTrailsLib.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;

namespace EnduroTrailsLib
{
    public class EnduroTrails
    {
        private readonly IServiceProvider _serviceProvider;

        public ITrailAnalyzer Analyzer;
        public EnduroTrails(string FilePath)
        {
            var config = new Config(FilePath);
            _serviceProvider = new ServiceCollection()
                .AddSingleton(config)
                .AddSingleton<IGpxReader, GpxReader>()
                .AddSingleton<ITrailAnalyzer, TrailAnalyzer>()
                .BuildServiceProvider();

            Analyzer = _serviceProvider.GetService<ITrailAnalyzer>();
        }
    }
}
