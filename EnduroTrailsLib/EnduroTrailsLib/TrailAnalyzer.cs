﻿using EnduroTrails.DataReader;
using EnduroTrails.Models;
using EnduroTrailsLib.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EnduroTrailsLib
{
    public class TrailAnalyzer : ITrailAnalyzer
    {
        private readonly IGpxReader _gpx;
        private readonly Config _config;
        private readonly IList<GpxPoint> _gpxPoints;
        private const double R = 6373;
        private const double D2R = (Math.PI / 180.0);

        public TrailAnalyzer(IGpxReader gpx, Config config)
        {
            _gpx = gpx;
            _config = config;
            _gpxPoints = new List<GpxPoint>(GetGpxData());
        }

        public ICollection<GpxPoint> GetGpxData()
        {
            return _gpx.GetWaypointsList(_config.FilePath);
        }

        #region Distance
        public double GetDistance()
        {
            return GetDistanceInternal();
        }

        public double GetClimbingDistance()
        {
            return GetDistanceInternal((x, y) => x.Ele > y.Ele);
        }

        public double GetDescentDistance()
        {
            return GetDistanceInternal((x, y) => x.Ele < y.Ele);
        }

        private double GetDistanceInternal(Func<GpxPoint, GpxPoint, bool> predicate = null)
        {
            double distance = 0;

            for (int i = 1; i < _gpxPoints.Count; i++)
            {
                if (predicate == null || predicate(_gpxPoints[i], _gpxPoints[i - 1]))
                {
                    distance += GetDistance2Points(_gpxPoints[i], _gpxPoints[i - 1]);
                }
            }

            return Math.Round(distance, 2);
        }

        public double GetFlatDistance()
        {
            return GetDistanceInternal((x,y) => x.Ele == y.Ele);
        }
        #endregion

        #region Speed
        public double GetMinSpeed()
        {
            var waypointsAtDay = _gpxPoints
                .GroupBy(x => x.Time.Date)
                .Select(x => new { x.Key, points = x.ToArray() });

            double minSpeed = 2000;

            foreach (var day in waypointsAtDay)
            {
                for (int i = 1; i < day.points.Length; i++)
                {
                    double t = (day.points[i].Time - day.points[i - 1].Time).TotalHours;
                    double s = GetDistance2Points(day.points[i - 1], day.points[i]);
                    double speed = s / t;

                    minSpeed = Math.Min(speed, minSpeed);
                }
            }

            return Math.Round(minSpeed, 2);
        }

        public double GetMaxSpeed()
        {

            var waypointsAtDay = _gpxPoints
                .GroupBy(x => x.Time.Date)
                .Select(x => new { x.Key, points = x.ToArray() });

            double maxSpeed = 0;

            foreach (var day in waypointsAtDay)
            {
                for (int i = 1; i < day.points.Length; i++)
                {
                    double t = (day.points[i].Time - day.points[i - 1].Time).TotalHours;
                    double s = GetDistance2Points(day.points[i - 1], day.points[i]);
                    double speed = s / t;

                    maxSpeed = speed > maxSpeed ? speed : maxSpeed;
                }
            }

            return Math.Round(maxSpeed, 2);
        }

        public double GetAvgSpeed()
        {
            var waypointsAtDay = _gpxPoints
                .ToList()
                .GroupBy(x => x.Time.Date)
                .Select(x => new { x.Key, points = x.ToArray() });

            double tempSpeed = 0;

            foreach (var day in waypointsAtDay)
            {
                for (int i = 1; i < day.points.Length; i++)
                {
                    double t = (day.points[i].Time - day.points[i - 1].Time).TotalHours;
                    double s = GetDistance2Points(day.points[i - 1], day.points[i]);
                    double speed = s / t;

                    tempSpeed += speed;
                }
            }

            return Math.Round(tempSpeed / (_gpxPoints.Count() - 1), 2);
        }

        #endregion

        #region Elevation

        public double GetMinElev()
        {
            return _gpxPoints.Min(x => x.Ele);
        }

        public double GetAvgElev()
        {
            double totalElev = 0;

            foreach (var item in _gpxPoints)
            {
                totalElev += item.Ele;
            }

            return Math.Round(totalElev / _gpxPoints.Count, 2);
        }

        public double GetTotalClimbing()
        {
            double totalClimbing = 0;

            for (int i = 1; i < _gpxPoints.Count(); i++)
            {
                if (_gpxPoints[i].Ele > _gpxPoints[i - 1].Ele)
                {
                    totalClimbing += (_gpxPoints[i].Ele - _gpxPoints[i - 1].Ele);
                }
            }

            return Math.Round(totalClimbing, 2);
        }

        public double GetTotalDescent()
        {
            double totalClimbing = 0;

            for (int i = 1; i < _gpxPoints.Count(); i++)
            {
                if (_gpxPoints[i].Ele < _gpxPoints[i - 1].Ele)
                {
                    totalClimbing += (_gpxPoints[i - 1].Ele - _gpxPoints[i].Ele);
                }
            }

            return totalClimbing;
        }

        public double GetFinalBalance()
        {
            return _gpxPoints.Max(x => x.Ele) - _gpxPoints.Min(x => x.Ele);
        }

        #endregion

        #region Time
        public TimeSpan GetTotalTrackTime()
        {
            return _gpxPoints.Max(x => x.Time) - _gpxPoints.Min(x => x.Time);
        }

        public TimeSpan GetClimbingTime()
        {
            TimeSpan climbingTime = TimeSpan.Zero;

            for (int i = 1; i < _gpxPoints.Count(); i++)
            {
                if (_gpxPoints[i].Ele > _gpxPoints[i - 1].Ele)
                {
                    climbingTime += (_gpxPoints[i].Time - _gpxPoints[i - 1].Time);
                }
            }

            return climbingTime;
        }

        public TimeSpan GetDescTime()
        {
            TimeSpan descTime = TimeSpan.Zero;

            for (int i = 1; i < _gpxPoints.Count(); i++)
            {
                if (_gpxPoints[i].Ele < _gpxPoints[i - 1].Ele)
                {
                    descTime += (_gpxPoints[i].Time - _gpxPoints[i - 1].Time);
                }
            }

            return descTime;
        }

        public TimeSpan GetFlatTime()
        {
            TimeSpan flatTime = TimeSpan.Zero;

            for (int i = 1; i < _gpxPoints.Count(); i++)
            {
                if (_gpxPoints[i].Ele == _gpxPoints[i - 1].Ele)
                {
                    flatTime += (_gpxPoints[i].Time - _gpxPoints[i - 1].Time);
                }
            }

            return flatTime;
        }
        #endregion

        private double GetDistance2Points(GpxPoint w1, GpxPoint w2)
        {
            double distance = 0;

            double dlon = (w2.Lon - w1.Lon) * D2R;
            double dlat = (w2.Lat - w1.Lat) * D2R;

            double a = Math.Pow((Math.Sin(dlat / 2)), 2) + Math.Cos(w1.Lat * D2R) * Math.Cos(w2.Lat) * Math.Pow(Math.Sin(dlon / 2), 2);
            double c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
            distance += R * c;

            return Math.Round(distance, 5);
        }
    }
}
