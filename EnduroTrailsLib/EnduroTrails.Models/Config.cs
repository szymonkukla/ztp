﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EnduroTrails.Models
{
    public class Config
    {
        public string FilePath { get; set; }
        public Config(string filePath)
        {
            FilePath = filePath;
        }
    }
}
