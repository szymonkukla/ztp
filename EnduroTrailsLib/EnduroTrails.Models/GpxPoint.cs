﻿using System;

namespace EnduroTrails.Models
{
    public class GpxPoint
    {
        public double Lat { get; set; }
        public double Lon { get; set; }
        public double Ele { get; set; }
        public DateTime Time { get; set; }
    }
}
