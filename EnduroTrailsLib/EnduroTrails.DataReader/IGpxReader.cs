﻿using EnduroTrails.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace EnduroTrails.DataReader
{
    public interface IGpxReader
    {
        ICollection<GpxPoint> GetWaypointsList(string filePath);
    }
}
