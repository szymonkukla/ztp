﻿using EnduroTrails.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Xml.Linq;

namespace EnduroTrails.DataReader
{
    public class GpxReader : IGpxReader
    {
        public ICollection<GpxPoint> GetWaypointsList(string filePath)
        {
            var gpxFile = new XDocument();
            var gpxData = new List<GpxPoint>();
            XNamespace gpxNamespace = XNamespace.Get("http://www.topografix.com/GPX/1/1");

            try
            {
                gpxFile = XDocument.Load(filePath);
                gpxData = gpxFile.Descendants(gpxNamespace + "trkpt")
                .Select(waypoint => new GpxPoint
                {
                    Lat = Convert.ToDouble(waypoint.Attribute("lat")?.Value, CultureInfo.InvariantCulture),
                    Lon = Convert.ToDouble(waypoint.Attribute("lon")?.Value, CultureInfo.InvariantCulture),
                    Ele = Convert.ToDouble(waypoint.Element(gpxNamespace + "ele")?.Value, CultureInfo.InvariantCulture),
                    Time = Convert.ToDateTime(waypoint.Element(gpxNamespace + "time")?.Value, CultureInfo.InvariantCulture)
                }).ToList();
            }
            catch
            {
                throw;
            }

            return gpxData;
        }
    }
}
