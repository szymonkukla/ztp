﻿using System;
using System.Text;
using EnduroTrailsLib;

namespace EnduroLibTest
{
    class Program
    {
        static void Main(string[] args)
        {
            var trailsLib = new EnduroTrailsLib.EnduroTrails("twister.gpx");
            var trails = trailsLib.Analyzer;

            var resultTxt = $@"
            Twister:
            Total distance: {trails.GetDistance()} km
            Climbing distance: {trails.GetClimbingDistance()} km
            Descent distance: {trails.GetDescentDistance()} km
            Flat distance: {trails.GetFlatDistance()} km
            Min speed: {trails.GetMinSpeed()} km/h
            Max speed: {trails.GetMaxSpeed()} km/h
            Avg speed: {trails.GetAvgSpeed()} km/h
            Min Elev: {trails.GetMinElev()} m
            Avg Elev: {trails.GetAvgElev()} m
            Total Climbing: {trails.GetTotalClimbing()} m
            Total Desc: {trails.GetTotalDescent()} m
            Final Balance: {trails.GetFinalBalance()} m
            Total Track Time: {trails.GetTotalTrackTime()}
            Climbing Time: {trails.GetClimbingTime()}
            Desc Time: {trails.GetDescTime()}
            Flat Time: {trails.GetFlatTime()}
            ";

            Console.WriteLine(resultTxt);
            Console.ReadKey();
        }
    }
}
