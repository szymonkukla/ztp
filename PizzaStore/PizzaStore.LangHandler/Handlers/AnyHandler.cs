﻿using MongoDB.Bson;
using PizzaStore.Models.Pizza;
using PizzaStore.Repository;
using System.Collections.Generic;
using System.Linq;

namespace PizzaStore.LangHandler
{
    public class AnyHandler : BaseHandler, IAnyHandler
    {
        public AnyHandler(IRepository<Pizza> pizza) : base(pizza)
        {
        }

        public override PizzaDto HandleWork(string language, string id, bool isSuccessor = false)
        {
            var result = Pizza.GetAll().GetPizzaWithAnyLanguage(new ObjectId(id));

            return result ?? Successor?.HandleWork(string.Empty, id, true);
        }

        public override IEnumerable<PizzaDto> HandleWork(string language, bool isSuccessor = false)
        {
            var result = Pizza.GetAll().GetPizzasWithAnyLanguage().ToList();

            if (!result.Any())
                Successor?.HandleWork(language, true);

            return result;
        }
    }
}
