﻿using MongoDB.Bson;
using PizzaStore.Models.Pizza;
using PizzaStore.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace PizzaStore.LangHandler
{
    public class DesiredHandler : BaseHandler, IDesiredHandler
    {
        public DesiredHandler(IRepository<Pizza> pizza) : base(pizza)
        {
        }

        public override PizzaDto HandleWork(string language, string id, bool isSuccessor = false)
        {
            if (string.IsNullOrEmpty(language) || !Regex.IsMatch(language, "[a-zA-Z]{2}-[A-Z]{2}"))
                throw new ArgumentException(nameof(language));

            var result = Pizza.GetAll().GetPizzaWithLanguage(language, new ObjectId(id));

            return result ?? Successor?.HandleWork(language, id, true);
        }

        public override IEnumerable<PizzaDto> HandleWork(string language, bool isSuccessor = false)
        {
            if (string.IsNullOrEmpty(language) || !Regex.IsMatch(language, "[a-zA-Z]{2}-[A-Z]{2}"))
                throw new ArgumentException(nameof(language));

            var result = Pizza.GetAll().GetPizzasWithLanguage(language).ToList();

            return result.Any() ? result : Successor?.HandleWork(language, true);
        }
    }
}
