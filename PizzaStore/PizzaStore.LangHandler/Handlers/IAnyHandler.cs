﻿using PizzaStore.Models.Pizza;
using System;
using System.Collections.Generic;
using System.Text;

namespace PizzaStore.LangHandler
{
    public interface IAnyHandler : IBaseHandler
    {

    }

    public interface IDesiredHandler : IBaseHandler
    {
    }

    public interface IEnglishHandler : IBaseHandler
    {
    }

    public interface IPolishHandler : IBaseHandler
    {
    }
}
