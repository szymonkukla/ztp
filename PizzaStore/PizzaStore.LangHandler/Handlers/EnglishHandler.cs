﻿using MongoDB.Bson;
using PizzaStore.Models.Pizza;
using PizzaStore.Repository;
using System.Collections.Generic;
using System.Linq;

namespace PizzaStore.LangHandler
{
    public class EnglishHandler : BaseHandler, IEnglishHandler
    {
        public EnglishHandler(IRepository<Pizza> pizza) : base(pizza)
        {
        }

        public override PizzaDto HandleWork(string language, string id, bool isSuccessor = false)
        {
            if (isSuccessor)
                language = "en-GB";

            var result = Pizza.GetAll().GetPizzaWithLanguage(language, new ObjectId(id));

            return result ?? Successor?.HandleWork(language, id, true);
        }

        public override IEnumerable<PizzaDto> HandleWork(string language, bool isSuccessor = false)
        {
            if (isSuccessor)
                language = "en-GB";

            var result = Pizza.GetAll().GetPizzasWithLanguage(language).ToList();

            return result.Any() ? result : Successor?.HandleWork(language, true);
        }
    }
}
