﻿using PizzaStore.Models.Pizza;
using System;
using System.Collections.Generic;
using System.Text;

namespace PizzaStore.LangHandler
{
    public interface IBaseHandler
    {
        void SetSuccessor(IBaseHandler successor);
        PizzaDto HandleWork(string language, string id, bool isSuccessor = false);
        IEnumerable<PizzaDto> HandleWork(string language, bool isSuccessor = false);
    }
}
