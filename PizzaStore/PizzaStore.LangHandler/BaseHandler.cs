﻿using PizzaStore.Models.Pizza;
using PizzaStore.Repository;
using System.Collections.Generic;

namespace PizzaStore.LangHandler
{
    public abstract class BaseHandler : IBaseHandler
    {
        protected IBaseHandler Successor;
        protected readonly IRepository<Pizza> Pizza;

        public BaseHandler(IRepository<Pizza> pizza)
        {
            Pizza = pizza;
        }

        public void SetSuccessor(IBaseHandler successor)
        {
            Successor = successor;
        }

        public abstract PizzaDto HandleWork(string language, string id, bool isSuccessor = false);
        public abstract IEnumerable<PizzaDto> HandleWork(string language, bool isSuccessor = false);
    }
}
