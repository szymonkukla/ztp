﻿using MongoDB.Bson;
using PizzaStore.Models.Pizza;
using System.Collections.Generic;
using System.Linq;

namespace PizzaStore.LangHandler
{
    public  static class ExtensionMethods
    {
        public static PizzaDto GetPizzaWithLanguage(this IQueryable<Pizza> collection, string language, ObjectId id)
        {
            return collection.Where(p => p._Id == id && p.Contents.Any(c => c.LanguageCode.Equals(language)))
                                        .Select(x => new
                                        {
                                            x.ImageUrl, x.Price,
                                            Content = x.Contents.First(c => c.LanguageCode.Equals(language))
                                        })
                                        .ToArray()
                                        .Select(x => new PizzaDto
                                        {
                                            Price = x.Price,
                                            Description = x.Content.Description,
                                            ImageUrl = x.ImageUrl,
                                            Name = x.Content.Name,
                                            LanguageCode = x.Content.LanguageCode
                                        })
                                        .FirstOrDefault();
        }

        public static PizzaDto GetPizzaWithAnyLanguage(this IQueryable<Pizza> collection, ObjectId id)
        {
            return collection.Where(p => p._Id == id && p.Contents.Any())
                                        .Select(x => new
                                        {
                                            x.ImageUrl, x.Price,
                                            Content = x.Contents.First()
                                        })
                                        .ToArray()
                                        .Select(x => new PizzaDto
                                        {
                                            Price = x.Price,
                                            Description = x.Content.Description,
                                            ImageUrl = x.ImageUrl,
                                            Name = x.Content.Name,
                                            LanguageCode = x.Content.LanguageCode
                                        })
                                        .FirstOrDefault();
        }

        public static IEnumerable<PizzaDto> GetPizzasWithLanguage(this IQueryable<Pizza> collection, string language)
        {
            return collection.Where(p => p.Contents.Any(c => c.LanguageCode.Equals(language)))
                        .Select(x => new
                        {
                            x.ImageUrl,
                            x.Price,
                            Content = x.Contents.First(c => c.LanguageCode.Equals(language))
                        })
                        .ToArray()
                        .Select(x => new PizzaDto
                        {
                            Price = x.Price,
                            Description = x.Content.Description,
                            ImageUrl = x.ImageUrl,
                            Name = x.Content.Name,
                            LanguageCode = x.Content.LanguageCode
                        });
        }

        public static IEnumerable<PizzaDto> GetPizzasWithAnyLanguage(this IQueryable<Pizza> collection)
        {
            return collection.Where(p => p.Contents.Any())
                                        .Select(x => new
                                        {
                                            x.ImageUrl,
                                            x.Price,
                                            Content = x.Contents.First()
                                        })
                                        .ToArray()
                                        .Select(x => new PizzaDto
                                        {
                                            Price = x.Price,
                                            Description = x.Content.Description,
                                            ImageUrl = x.ImageUrl,
                                            Name = x.Content.Name,
                                            LanguageCode = x.Content.LanguageCode
                                        });
        }
    }
}
