﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PizzaStore.LangHandler;
using PizzaStore.Models.Pizza;
using PizzaStore.Repository;

namespace PizzaStore.Controllers
{
    [ApiController]
    public class PizzaController : ControllerBase
    {
        private readonly IDesiredHandler _desired;

        public PizzaController(IDesiredHandler desired, IEnglishHandler english, IPolishHandler polish, IAnyHandler any)
        {
            _desired = desired;
            _desired.SetSuccessor(english);
            english.SetSuccessor(polish);
            polish.SetSuccessor(any);
        }

        [HttpGet]
        [Route("api/{id}/{language}")]
        public ActionResult<PizzaDto> GetPizza(string id, string language)
        {
            return _desired.HandleWork(language, id);
        }

        [HttpGet]
        [Route("api/pizzas/{language}")]
        public ActionResult<List<PizzaDto>> GetPizzasList(string language)
        {
            return _desired.HandleWork(language).ToList();
        }
    }
}
