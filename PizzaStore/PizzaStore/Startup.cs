﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PizzaStore.LangHandler;
using PizzaStore.Models.Config;
using PizzaStore.Models.Pizza;
using PizzaStore.Repository;
using PizzaStore.Repository.DbFactory;

namespace PizzaStore
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var Builder = new ConfigurationBuilder()
                        .SetBasePath(env.ContentRootPath)
                        .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);

            Configuration = Builder.Build();
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var DbConfig = new DbConfig();
            Configuration.GetSection("MongoDb").Bind(DbConfig);

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddSingleton(DbConfig);
            services.AddSingleton<IDbFactory, DbFactory>();
            services.AddSingleton<IRepository<Pizza>, Repository<Pizza>>();

            services.AddSingleton<IDesiredHandler, DesiredHandler>();
            services.AddSingleton<IEnglishHandler, EnglishHandler>();
            services.AddSingleton<IPolishHandler, PolishHandler>();
            services.AddSingleton<IAnyHandler, AnyHandler>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();
        }
    }
}
