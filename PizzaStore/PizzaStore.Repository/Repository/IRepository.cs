﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace PizzaStore.Repository
{
    public interface IRepository<T>
    {
        ICollection<T> Get(Expression<Func<T, bool>> query);
        IQueryable<T> GetAll();
        void Add(T entity);
        void Delete(Expression<Func<T, bool>> queryExpression);
        void Update(Expression<Func<T, bool>> queryExpression, T entity);
    }
}
