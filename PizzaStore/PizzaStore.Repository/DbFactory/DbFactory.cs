﻿using MongoDB.Driver;
using PizzaStore.Models.Config;
using System;
using System.Collections.Generic;
using System.Text;

namespace PizzaStore.Repository.DbFactory
{
    public class DbFactory: IDbFactory
    {
        private readonly IMongoDatabase _db;

        public DbFactory(DbConfig dbDonfig)
        {
            var client = new MongoClient(dbDonfig.ConnectionsString);
            _db = client.GetDatabase(dbDonfig.DbName);
        }

        public IMongoDatabase GetDb()
        {
            return _db;
        }
    }
}
