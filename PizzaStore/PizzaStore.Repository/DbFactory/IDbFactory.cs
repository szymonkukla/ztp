﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;

namespace PizzaStore.Repository.DbFactory
{
    public interface IDbFactory
    {
        IMongoDatabase GetDb();
    }
}
