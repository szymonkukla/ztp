﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PizzaStore.Models.Config
{
    public class DbConfig
    {
        public string ConnectionsString { get; set; }
        public string DbName { get; set; }
    }
}
