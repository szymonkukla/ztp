﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PizzaStore.Models.Pizza
{
    public class PizzaDto
    {
        public double Price { get; set; }
        public string ImageUrl { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string LanguageCode { get; set; }
    }
}
