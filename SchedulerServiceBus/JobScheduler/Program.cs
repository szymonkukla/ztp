﻿using Hangfire;
using Hangfire.MemoryStorage;
using Scheduler.BusModels;
using ServiceBusHandler;
using System;

namespace JobScheduler
{
    internal class Program
    {
        private static IServiceBusHandler _serviceBus;
        private static BackgroundJobServer _jobServer;

        private static void Main(string[] args)
        {
            _serviceBus = new ServiceBusHandler.ServiceBusHandler();

            GlobalConfiguration.Configuration.UseMemoryStorage();

            using (_jobServer = new BackgroundJobServer())
            {
                PrepareJob();
                SubscribeKillCall();
                Console.ReadLine();
            }
        }

        public static void SendSchedule()
        {
            Console.WriteLine("Schedule Execute {0}", DateTime.Now.ToString());
            _serviceBus.SendMessage(new Message(EMessageKeys.CheckData, null));
        }

        private static void PrepareJob(string cron = "* * * * *")
        {
            RecurringJob.AddOrUpdate("JobSchedulerMainTask", () => SendSchedule(), cron, TimeZoneInfo.Utc);
        }

        private static void SubscribeKillCall()
        {
            _serviceBus.Subscribe<Message>((m) =>
            {
                if (m.MessageKey == EMessageKeys.StopScheduler)
                {
                    _jobServer.Dispose();
                    Environment.Exit(0);
                }
            }).Wait();

        }
    }
}
