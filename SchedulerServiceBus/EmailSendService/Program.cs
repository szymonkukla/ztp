﻿using EmailSender;
using Microsoft.Extensions.DependencyInjection;
using Scheduler.Models.Config;
using ServiceBusHandler;
using System;

namespace EmailSendService
{
    class Program
    {
        private static void Main(string[] args)
        {
            var smtp = new SmtpSettingsModel
            {
                SenderEmail = "some@sender.com",
                SenderName = "Some Sender",
                SmtpHost = "smtp.mailtrap.io",
                SmtpPort = 2525,
                Login = "b99e6fc2640266",
                Pass = "0c1e1fbb34c078"
            };

            var serviceProvider = new ServiceCollection()
                .AddSingleton<IServiceBusHandler, ServiceBusHandler.ServiceBusHandler>()
                .AddSingleton(smtp)
                .AddScoped<IEmailSender, EmailSender.EmailSender>()
                .AddScoped<ISendHandler, SendHandler>()
                .BuildServiceProvider();

            var handler = serviceProvider.GetService<ISendHandler>();
            handler.RunSendHandler();
        }
    }
}
