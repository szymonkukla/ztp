﻿using EmailSender;
using Scheduler.BusModels;
using Scheduler.Models;
using ServiceBusHandler;
using System;
using System.Collections.Generic;
using System.Text;

namespace EmailSendService
{
    public interface ISendHandler
    {
        void RunSendHandler();
    }
    public class SendHandler : ISendHandler
    {
        private readonly IEmailSender _emailSender;
        private readonly IServiceBusHandler _serviceBus;

        public SendHandler(IEmailSender emailSender, IServiceBusHandler serviceBus)
        {
            _emailSender = emailSender;
            _serviceBus = serviceBus;
        }

        public void RunSendHandler()
        {
            _serviceBus.Subscribe<Message<EmailModel>>(m => SendEmail(m.Content)).Wait();
        }

        private async void SendEmail(EmailModel email)
        {
            Console.WriteLine("{0}: Sending email to {1}", DateTime.Now, email.RecipientEmail);
            await _emailSender.SendEmailAsync(email);
        }
    }
}
