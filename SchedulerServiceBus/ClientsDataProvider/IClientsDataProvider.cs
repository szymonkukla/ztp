﻿using Scheduler.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ClientsDataProvider
{
    public interface IClientsDataProvider
    {
        IEnumerable<DiscountMessage> GetAllMessages();
        IEnumerable<DiscountMessage> GetMessagesFromIndex(int index = 0, int maxCount = 0);
    }
}
