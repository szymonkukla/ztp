﻿using CsvRepository;
using Scheduler.Models;
using System;
using System.Collections.Generic;

namespace ClientsDataProvider
{
    public class ClientsDataProvider : IClientsDataProvider
    {
        private ICsvRepository<DiscountMessage> _discRepo;

        public ClientsDataProvider(ICsvRepository<DiscountMessage> discRepo)
        {
            _discRepo = discRepo;
        }

        public IEnumerable<DiscountMessage> GetAllMessages()
        {
            return _discRepo.GetAllRecords();
        }
        public IEnumerable<DiscountMessage> GetMessagesFromIndex(int index = 0, int maxCount = 0)
        {
            return _discRepo.GetRecordsFromIndex(index, maxCount);
        }
    }
}
