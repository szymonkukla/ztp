﻿using System;
using System.Threading.Tasks;

namespace ServiceBusHandler.BusServiceFactory
{
    public interface IBusServiceFactory
    {
        Task SendMessage<T>(T message);
        Task Subscribe<T>(Action<T> function);
    }
}
