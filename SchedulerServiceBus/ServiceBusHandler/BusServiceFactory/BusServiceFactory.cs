﻿using RawRabbit;
using System;
using System.Threading.Tasks;

namespace ServiceBusHandler.BusServiceFactory
{
    internal class BusServiceFactory : IBusServiceFactory
    {
        private IBusClient _busClient;

        public BusServiceFactory(IBusClient busClient)
        {
            _busClient = busClient;
        }

        public async Task SendMessage<T>(T message)
        {
            await _busClient.PublishAsync(message);
        }

        public Task Subscribe<T>(Action<T> function)
        {
            return _busClient.SubscribeAsync<T>(
                async message => function(message)
            );
        }
    }
}
