﻿using Microsoft.Extensions.DependencyInjection;
using RawRabbit;
using RawRabbit.Configuration;
using RawRabbit.DependencyInjection.ServiceCollection;
using RawRabbit.Instantiation;
using ServiceBusHandler.BusServiceFactory;
using System;
using System.Threading.Tasks;

namespace ServiceBusHandler
{
    public class ServiceBusHandler : IServiceBusHandler
    {
        private readonly IServiceProvider _serviceProvider;
        private IBusServiceFactory _serviceFactory;

        public ServiceBusHandler(RawRabbitConfiguration config = null)
        {
            _serviceProvider = new ServiceCollection()
                .AddRawRabbit(new RawRabbitOptions
                {
                    ClientConfiguration = config ?? new RawRabbitConfiguration
                    {
                        Username = "guest",
                        Password = "guest",
                        Port = 5672,
                        VirtualHost = "/",
                        Hostnames = { "localhost" }
                    }
                })
                .AddSingleton<IBusServiceFactory, BusServiceFactory.BusServiceFactory>()
                .BuildServiceProvider();

            _serviceFactory = _serviceProvider.GetService<IBusServiceFactory>(); ;
        }

        public Task SendMessage<T>(T message) {
            return _serviceFactory.SendMessage(message);
        }

        public Task Subscribe<T>(Action<T> function)
        {
            return _serviceFactory.Subscribe(function);
        }
    }
}
