﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduler.BusModels
{
    public enum EMessageKeys
    {
        CheckData,
        StopScheduler,
        SendEmail,
    }
}
