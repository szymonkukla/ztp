﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduler.BusModels
{
    public class Message<T>
    {
        public string MessageKey { get; }
        public T Content { get; }

        public Message(T content, string messageKey = null)
        {
            MessageKey = string.IsNullOrEmpty(messageKey) ? typeof(T).Name : messageKey;
            Content = content;
        }
    }

    public class Message
    {
        public EMessageKeys MessageKey { get; }
        public string Content { get; }

        public Message(EMessageKeys messageKey, string content = null)
        {
            MessageKey = messageKey;
            Content = content;
        }
    }
}
