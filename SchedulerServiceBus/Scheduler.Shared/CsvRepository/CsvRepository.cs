﻿using CsvHelper;
using Scheduler.Models.Config;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace CsvRepository
{
    public class CsvRepository<T> : ICsvRepository<T>
    {
        private CsvSettingsModel _settings = new CsvSettingsModel();
        private CsvHelper.Configuration.Configuration _csvConfig = new CsvHelper.Configuration.Configuration();

        public CsvRepository(CsvSettingsModel config)
        {
            _settings = config;
            _csvConfig.HasHeaderRecord = _settings.FileHasHeaders;
        }
        public IEnumerable<T> GetAllRecords(string fileName = null)
        {
            if (string.IsNullOrEmpty(fileName))
                fileName = _settings.FilePath;

            using (var sr = File.OpenText(fileName))
            {
                var reader = new CsvReader(sr, _csvConfig);
                return reader.GetRecords<T>().ToArray();
            }
        }

        public IEnumerable<T> GetRecordsFromIndex(int startIndex, int maxCount = 0, string fileName = null)
        {
            if (string.IsNullOrEmpty(fileName))
                fileName = _settings.FilePath;

            using (var sr = File.OpenText(fileName))
            {
                var reader = new CsvReader(sr, _csvConfig);
                var set = reader.GetRecords<T>().Skip(startIndex);
                if (maxCount > 0)
                    set = set.Take(maxCount);

                return set.ToArray();
            }
        }
    }
}
