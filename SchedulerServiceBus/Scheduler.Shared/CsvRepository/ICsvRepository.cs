﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CsvRepository
{
    public interface ICsvRepository<T>
    {
        IEnumerable<T> GetAllRecords(string fileName = null);
        IEnumerable<T> GetRecordsFromIndex(int startIndex, int maxCount = 0, string fileName = null);
    }
}
