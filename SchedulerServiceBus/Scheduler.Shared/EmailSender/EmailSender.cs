﻿using EmailSender.SendHandler;
using FluentEmail.Core;
using Microsoft.Extensions.DependencyInjection;
using Scheduler.Models;
using Scheduler.Models.Config;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace EmailSender
{
    public class EmailSender : IEmailSender
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly SmtpSettingsModel _config;
        private readonly SendEmailHandler _sendHandler;
        public EmailSender(SmtpSettingsModel config)
        {
            _config = config;

            _serviceProvider = new ServiceCollection()
                .AddLogging()
                .AddFluentEmail(config.SenderEmail, config.SenderName)
                .AddRazorRenderer()
                .AddSmtpSender(PrepareSmtpClient()).Services
                .AddScoped<SendEmailHandler>()
                .BuildServiceProvider();

            _sendHandler = _serviceProvider.GetService<SendEmailHandler>();
        }

        public void SendEmail(EmailModel emailData)
        {
            _sendHandler.SendEmail(emailData);
        }

        public Task SendEmailAsync(EmailModel emailData)
        {
            return _sendHandler.SendEmailAsync(emailData);
        }

        public Task SendManyMessages(IEnumerable<EmailModel> messages)
        {
            return _sendHandler.SendManyMessagesAsync(messages);
        }

        private SmtpClient PrepareSmtpClient()
        {
            return new SmtpClient
            {
                Host = _config.SmtpHost,
                Port = _config.SmtpPort,
                EnableSsl = true,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(_config.Login, _config.Pass),
                DeliveryMethod = SmtpDeliveryMethod.Network,
            };

        }
    }
}
