﻿using Scheduler.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EmailSender
{
    public interface IEmailSender
    {
        void SendEmail(EmailModel emailData);
        Task SendEmailAsync(EmailModel emailData);
        Task SendManyMessages(IEnumerable<EmailModel> messages);
    }
}
