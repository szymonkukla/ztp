﻿using Hangfire;
using Hangfire.Storage;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JobScheduler.HangFireProvider
{
    class JobsProvider<T> where T: IJobHandler
    {
        private ILogger _logger;
        private readonly string _jobId = "DiscountCheckJob";
        public JobsProvider(ILogger<JobsProvider<T>> logger)
        {
            _logger = logger;
        }

        public void RunSyncJob(T handler, int nextIndex, string crontab)
        {
            var isJob = IsJobExist(_jobId);

            if (nextIndex == 0 && isJob)
            {
                _logger.LogInformation($"Job exist: {_jobId}");
                return;
            }
            _logger.LogInformation($"Updating job: {_jobId} with index: {nextIndex}");
            RecurringJob.AddOrUpdate(_jobId, () => handler.ActivateJob(nextIndex), crontab, TimeZoneInfo.Utc);
        }

        private bool IsJobExist(string jobId)
        {
            var isExists = false;
            using (var connection = JobStorage.Current.GetConnection())
            {
                var recurringJobs = connection.GetRecurringJobs();
                isExists = recurringJobs.Any(j => j.Id == jobId && !j.Removed);
            }
            return isExists;
        }
    }
}
