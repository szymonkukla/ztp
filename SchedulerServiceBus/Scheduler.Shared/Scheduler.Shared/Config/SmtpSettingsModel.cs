﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduler.Models.Config
{
    public class SmtpSettingsModel
    {
        public string SenderEmail { get; set; }
        public string SenderName { get; set; }
        public string SmtpHost { get; set; }
        public int SmtpPort { get; set; }
        public string Login { get; set; }
        public string Pass { get; set; }
    }
}
