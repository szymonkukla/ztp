﻿using ClientsDataProvider;
using EmailHandler.Interfaces;
using Scheduler.BusModels;
using Scheduler.Models;
using ServiceBusHandler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmailHandler
{
    public class EmailHandler : IEmailHandler
    {
        private readonly IClientsDataProvider _clientsData;
        private readonly IServiceBusHandler _busService;
        private static int _lastIndex = 0;
        public EmailHandler(IClientsDataProvider clientsData, IServiceBusHandler busService)
        {
            _clientsData = clientsData;
            _busService = busService;
        }

        public void RunEmailHandler()
        {
            _busService.Subscribe<Message>((m) =>
            {
                if (m.MessageKey == EMessageKeys.CheckData)
                {
                    HandleNewClients();
                }
            }).Wait();
        }

        public void HandleNewClients()
        {
            Console.WriteLine("{0}: Checking Clients...", DateTime.Now);
            var clients = _clientsData.GetMessagesFromIndex(_lastIndex, 10);
            if (clients != null &&  clients.Any()) {
                Console.WriteLine("{0}: New clients count: {1}", DateTime.Now, clients.Count());
                _lastIndex += clients.Count();

                var emails = PrepareMessages(clients);
                Task.WhenAll(emails.Select(e => _busService.SendMessage(new Message<EmailModel>(e))));
            }
        }
        private IEnumerable<EmailModel> PrepareMessages(IEnumerable<DiscountMessage> discounts)
        {
            return discounts.Select(d =>
                new EmailModel
                {
                    RecipientEmail = d.Email,
                    RecipientName = $"{d.FirstName} {d.LastName}",
                    Subject = "Mega discount, specially for You!",
                    TemplatePath = "DiscountMailTemplate.cshtml",
                    TemplateModel = d
                }
            );
        }
    }
}
