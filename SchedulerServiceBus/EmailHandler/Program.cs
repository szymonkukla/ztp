﻿using ClientsDataProvider;
using CsvRepository;
using EmailHandler.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using Scheduler.BusModels;
using Scheduler.Models;
using Scheduler.Models.Config;
using ServiceBusHandler;

namespace EmailHandler
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var serviceProvider = new ServiceCollection()
                .AddSingleton<IServiceBusHandler, ServiceBusHandler.ServiceBusHandler>()
                .AddSingleton(new CsvSettingsModel
                {
                    FilePath = "./contactlist.csv",
                    FileHasHeaders = false
                })
                .AddScoped<ICsvRepository<DiscountMessage>, CsvRepository<DiscountMessage>>()
                .AddScoped<IClientsDataProvider, ClientsDataProvider.ClientsDataProvider>()
                .AddScoped<IEmailHandler, EmailHandler>()
                .BuildServiceProvider();

            var handler = serviceProvider.GetService<IEmailHandler>();
            handler.RunEmailHandler();
        }
    }
}
