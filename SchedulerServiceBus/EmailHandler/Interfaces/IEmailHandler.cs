﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EmailHandler.Interfaces
{
    interface IEmailHandler
    {
        void RunEmailHandler();
        void HandleNewClients();
    }
}
