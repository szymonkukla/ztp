﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduler.Models
{
    public class DiscountMessage
    {
        public string Email { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public int Discount { get; set; }
        public DateTime DiscountEnd { get; set; }
    }
}
