﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduler.Models.Config
{
    public class SchedulerSettingsModel
    {
        public string SendInterval { get; set; }
        public int EmailsPerInterval { get; set; }
    }
}
