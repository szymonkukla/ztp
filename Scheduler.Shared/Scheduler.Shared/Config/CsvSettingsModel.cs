﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduler.Models.Config
{
    public class CsvSettingsModel
    {
        public string FilePath { get; set; }
        public bool FileHasHeaders { get; set; }
    }
}
