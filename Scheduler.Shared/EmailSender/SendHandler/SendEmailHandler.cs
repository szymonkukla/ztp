﻿using FluentEmail.Core;
using Microsoft.Extensions.Logging;
using Scheduler.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmailSender.SendHandler
{
    public class SendEmailHandler
    {
        private IFluentEmail _fluentEmail;
        private IFluentEmailFactory _fluentEmailFactory;

        public SendEmailHandler(IFluentEmail fluentEmail, IFluentEmailFactory fluentEmailFactory, ILogger<SendEmailHandler> logger)
        {
            _fluentEmail = fluentEmail;
            _fluentEmailFactory = fluentEmailFactory;
        }
        public void SendEmail(EmailModel emailData)
        {
            var email = PrepareMessage(emailData);
            email.Send();
        }

        public Task SendEmailAsync(EmailModel emailData)
        {
            var email = PrepareMessage(emailData);
            return email.SendAsync();
        }

        public async Task SendManyMessagesAsync(IEnumerable<EmailModel> messages)
        {
            await Task.WhenAll(messages.Select(m => _fluentEmail.To(m.RecipientEmail, m.RecipientName)
                            .Subject(m.Subject)
                            .UsingTemplateFromFile($"{Directory.GetCurrentDirectory()}/EmailTemplates/{m.TemplatePath}", m.TemplateModel)
                            .SendAsync()
                            ));
        }

        private IFluentEmail PrepareMessage(EmailModel emailData)
        {
            return _fluentEmail
                    .To(emailData.RecipientEmail, emailData.RecipientName)
                    .Subject(emailData.Subject)
                    .UsingTemplateFromFile($"{Directory.GetCurrentDirectory()}/EmailTemplates/{emailData.TemplatePath}", emailData.TemplateModel);
        }
    }
}
