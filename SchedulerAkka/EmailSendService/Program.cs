﻿using Akka.Actor;
using Scheduler.Core.Actors;
using Scheduler.Core.Messages;
using Scheduler.Models;
using System;

namespace EmailSendService
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var system = ActorSystem.Create("EmailSendService", Akka.Configuration.ConfigurationFactory.Load()))
            {
                var actor = system.ActorOf(Props.Create(() => new EmailSenderActor()), "EmailSendService");
                actor.Tell(new SendEmailMessage { Content = new EmailModel { RecipientEmail = "abc@fsda"} });
                system.WhenTerminated.Wait();
            }
        }
    }
}
