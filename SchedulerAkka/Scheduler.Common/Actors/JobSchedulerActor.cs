﻿using Akka.Actor;
using Scheduler.Core.Messages;
using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduler.Core.Actors
{
    public class JobSchedulerActor : ReceiveActor
    {
        private ICancelable _cancelable;
        private readonly TimeSpan _interval = TimeSpan.FromSeconds(5);
        private IActorRef mailHandler;
        public JobSchedulerActor(IServiceProvider serviceProvider)
        {
            mailHandler = Context.ActorOf(Props.Create<EmailHandlerActor>(serviceProvider), "EmailHandlerActor");
            Receive<SystemStartMessage>(message => RunScheduler());
        }

        private void RunScheduler()
        {
            //var handlerActor = Context.ActorSelection("EmailHandlerActor");
            _cancelable = Context.System.Scheduler.ScheduleTellRepeatedlyCancelable(
                TimeSpan.Zero, _interval, mailHandler, new CheckClientsMessage(), Self);
        }
    }
}
