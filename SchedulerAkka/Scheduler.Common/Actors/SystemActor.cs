﻿using Akka.Actor;
using Scheduler.Core.Messages;
using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduler.Core.Actors
{
    public class SystemActor : ReceiveActor
    {
        private IServiceProvider _serviceProvider;
        public SystemActor(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
            Receive<SystemStartMessage>(message => RunScheduler(message));
        }

        private void RunScheduler(SystemStartMessage message)
        {
            Context.ActorOf(Props.Create<JobSchedulerActor>(_serviceProvider), "JobSchedulerActor").Tell(message);
            var emailHandler = Context.ActorOf(Props.Create<EmailSenderActor>(_serviceProvider), "EmailSenderActor");
        }
    }
}
