﻿using Akka.Actor;
using CsvRepository;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Scheduler.Core.Messages;
using Scheduler.Models;
using Scheduler.Models.Config;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Scheduler.Core.Actors
{
    public class EmailHandlerActor : ReceiveActor, IEmailHandlerActor
    {
        private ILogger _logger;
        private ICsvRepository<DiscountMessage> _csvRepo;
        private int _lastIndex = 0;
        private IActorRef senderActor;

        public EmailHandlerActor(IServiceProvider serviceProvider)
        {
            _csvRepo = serviceProvider.GetService<ICsvRepository<DiscountMessage>>();
            senderActor = Context.ActorOf<>
            Receive<CheckClientsMessage>(x => PrepareDiscountMessage(x));
        }

        private void PrepareDiscountMessage(CheckClientsMessage message)
        {
            var clients = _csvRepo.GetRecordsFromIndex(_lastIndex, 10);
            if (clients != null && clients.Any())
            {
                //var senderActor = Context.ActorSelection("/user/$a/EmailSenderActor");

                _lastIndex += clients.Count();
                var emails = PrepareMessages(clients);
                foreach (var e in emails)
                {
                    Console.WriteLine("Interval {0}", _lastIndex);
                    senderActor.Tell(new SendEmailMessage(e));
                }
            }
        }

        private IEnumerable<EmailModel> PrepareMessages(IEnumerable<DiscountMessage> discounts)
        {
            return discounts.Select(d =>
                new EmailModel
                {
                    RecipientEmail = d.Email,
                    RecipientName = $"{d.FirstName} {d.LastName}",
                    Subject = "Mega discount, specially for You!",
                    TemplatePath = "DiscountMailTemplate.cshtml",
                    TemplateModel = d
                }
            );
        }
    }
}
