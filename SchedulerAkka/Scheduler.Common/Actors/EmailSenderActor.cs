﻿using Akka.Actor;
using EmailSender;
using Microsoft.Extensions.DependencyInjection;
using Scheduler.Core.Messages;
using Scheduler.Models;
using Scheduler.Models.Config;
using Serilog;
using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduler.Core.Actors
{
    public class EmailSenderActor : ReceiveActor
    {
        private IEmailSender _emailSender;
        private ILogger _logger;
        public EmailSenderActor(IServiceProvider serviceProvider)
        {
            _emailSender = serviceProvider.GetService<IEmailSender>();
            _logger = serviceProvider.GetService<ILogger>();
            Receive<SendEmailMessage>(message => SendEmail(message.Content));
        }

        private async void SendEmail(EmailModel email)
        {
            _logger.Information($"Sending email to {email.RecipientEmail}");
            await _emailSender.SendEmailAsync(email);
        }
    }
}
