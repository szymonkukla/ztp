﻿using Scheduler.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduler.Core.Messages
{
    public class SendEmailMessage
    {
        public EmailModel Content { get; set; }
        public SendEmailMessage(EmailModel email)
        {
            Content = email;
        }
    }
}
