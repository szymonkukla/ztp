﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduler.Core.Messages
{
    public class ClientIndexChangeMessage
    {
        public int Index { get; set; }

        public ClientIndexChangeMessage(int index)
        {
            Index = index;
        }
    }
}
