﻿using Akka.Actor;
using Akka.Routing;
using CsvRepository;
using EmailSender;
using Microsoft.Extensions.DependencyInjection;
using Scheduler.Core.Actors;
using Scheduler.Core.Messages;
using Scheduler.Models;
using Scheduler.Models.Config;
using Serilog;
using Serilog.Events;
using System;
using System.IO;

namespace EmailHandler
{
    class Program
    {
        public static IServiceProvider ServiceProvider;
        private static IActorRef _actor;

        static void Main(string[] args)
        {
            

            var csvConfig = new CsvSettingsModel
            {
                FilePath = "./contactlist.csv",
                FileHasHeaders = false
            };

            var smtp = new SmtpSettingsModel
            {
                SenderEmail = "some@sender.com",
                SenderName = "Some Sender",
                SmtpHost = "smtp.mailtrap.io",
                SmtpPort = 2525,
                Login = "b99e6fc2640266",
                Pass = "0c1e1fbb34c078"
            };


            ServiceProvider = new ServiceCollection()
                //.AddLogging()
                .AddSingleton(BuildLogger())
                .AddSingleton(csvConfig)
                .AddSingleton(smtp)
                .AddScoped<ICsvRepository<DiscountMessage>, CsvRepository<DiscountMessage>>()
                .AddScoped<IEmailSender, EmailSender.EmailSender>()
                .BuildServiceProvider();

            using (var system = ActorSystem.Create("EmailScheduler", Akka.Configuration.ConfigurationFactory.Load()))
            {
                system.ActorOf(Props.Create(() => new SystemActor(ServiceProvider))).Tell(new SystemStartMessage());
                Log.Information("System Created");

                system.WhenTerminated.Wait();
            }
        }

        public static ILogger BuildLogger()
        {
            return new LoggerConfiguration()
            .MinimumLevel.Information()
            .MinimumLevel.Override("Microsoft", LogEventLevel.Warning)
            .Enrich.FromLogContext()
            .WriteTo.RollingFile(Path.Combine("log-{Date}.txt"))
            .CreateLogger();
        }
    }
}
